#define DEVICE_NAME ""
#define MIN_TIME 300          //Min. amount of time that must pass in ms before it will transmit again
//WIFI
//#define SSID "" //Not used atm
//#define WIFI_PASSWORD "" // Not used atm
#define AP_PASSWORD "ESP-8266"

//MQTT
#define PUB_PREFIX "rf"
#define SUB_PREFIX "rf"
#define SERVER "0.0.0.0"
#define PORT "1883"
#define USERNAME ""
#define PASSWORD ""

// DEFINE pins here
#define BUTTON_PIN D2
#define RF_RX D1
#define RF_TX D0

// Some devices are not able to use serial comms and all their pins. This is mostly
// refering to the esp-01 as 2 of the gpio pins are used for rx/tx communication.
#define USESERIAL
// If you are having issues enabling debug may help, extra information is sent
// over serial. It also makes it so there is no password for the ap. If debug is
// enabled but USESERIAL is not, then the extra information will not be sent over serial
#define DEBUG



#ifdef USESERIAL
  #define DEBUGLN(x) Serial.println(x)
  #define DEBUGPR(x) Serial.print(x);
#else
  #define DEBUGLN(x)
  #define DEBUGPR(x)
#endif
